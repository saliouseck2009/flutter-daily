import 'package:flutter_bloc/flutter_bloc.dart';
import '../cubit/counter.dart';

enum CounterEvent { INCREMENT, DECREMENT }

class CounterBloc extends Bloc<CounterEvent, Counter> {
  CounterBloc() : super(Counter(counter: 0));
  @override
  Stream<Counter> mapEventToState(CounterEvent event) async* {
    // TODO: implement mapEventToState
    print("event declenche before");
    switch (event) {
      case CounterEvent.INCREMENT:
        yield Counter(counter: state.counter + 1);
        break;

      case CounterEvent.DECREMENT:
        yield Counter(counter: state.counter - 1);
        break;
      default:
        print("event declenche");
        break;
    }
    //throw UnimplementedError();
  }
}
