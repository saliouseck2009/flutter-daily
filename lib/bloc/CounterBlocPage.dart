import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/CounterBloc.dart';
import '../cubit/counter.dart';

class CounterBlocPage extends StatelessWidget {
  const CounterBlocPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('bloc test'),
        ),
        body: Center(
            child: Column(
          children: [
            BlocBuilder<CounterBloc, Counter>(builder: (context, state) {
              return Column(
                children: [
                  Text(state.counter.toString()),
                ],
              );
            })
          ],
        )),
        floatingActionButton: Row(
          children: [
            FloatingActionButton(
              onPressed: () =>
                  context.read<CounterBloc>().add(CounterEvent.INCREMENT),
              tooltip: 'increment',
              child: Icon(Icons.add),
            ),
            Padding(padding: EdgeInsets.only(right: 15)),
            FloatingActionButton(
              onPressed: () =>
                  context.read<CounterBloc>().add(CounterEvent.DECREMENT),
              tooltip: 'decrement',
              child: Icon(Icons.remove),
            ),
          ],
        ));
  }
}
