import 'counter.dart';
import 'package:bloc/bloc.dart';

class CounterCubit extends Cubit<Counter> {
  CounterCubit() : super(Counter(counter: 0));
  void increment() =>
      emit(Counter(counter: state.counter + 1, message: 'incrementé'));
  void decrement() =>
      emit(Counter(counter: state.counter - 1, message: 'décrémenté'));
}
