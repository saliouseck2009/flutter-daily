import 'package:flutter/foundation.dart';

class Counter {
  int counter;
  String message;
  Counter({@required this.counter, this.message});
}
