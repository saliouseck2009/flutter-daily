import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'CounterCubit.dart';
import 'counter.dart';

class CounterPage extends StatelessWidget {
  const CounterPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('bloc cubit test'),
        ),
        body: Center(
            child: Column(
          children: [
            BlocBuilder<CounterCubit, Counter>(builder: (context, state) {
              return Text(state.counter.toString());
            })
          ],
        )),
        floatingActionButton: Row(
          children: [
            FloatingActionButton(
              onPressed: () =>
                  BlocProvider.of<CounterCubit>(context).increment(),
              tooltip: 'increment',
              child: Icon(Icons.add),
            ),
            Padding(padding: EdgeInsets.only(right: 15)),
            FloatingActionButton(
              onPressed: () =>
                  BlocProvider.of<CounterCubit>(context).decrement(),
              tooltip: 'decrement',
              child: Icon(Icons.remove),
            ),
          ],
        ));
  }
}
